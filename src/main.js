import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router/index'
import 'animate.css'
import ScrollAnimation from './directives/scrollanimation.js'



Vue.directive('scrollanimation', ScrollAnimation);

Vue.config.productionTip = false

//App.use(
  //createGtm({
    //id: 'G-DJS3EN2G3X', // Your GTM single container ID, array of container ids ['GTM-xxxxxx', 'GTM-yyyyyy'] or array of objects [{id: 'GTM-xxxxxx', queryParams: { gtm_auth: 'abc123', gtm_preview: 'env-4', gtm_cookies_win: 'x'}}, {id: 'GTM-yyyyyy', queryParams: {gtm_auth: 'abc234', gtm_preview: 'env-5', gtm_cookies_win: 'x'}}], // Your GTM single container ID or array of container ids ['GTM-xxxxxx', 'GTM-yyyyyy']
  //})
//);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
