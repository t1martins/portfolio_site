import Vue from 'vue';
import VueRouter from 'vue-router';
import home from '@/components/home.vue';
import about from '@/components/about.vue';
import work from '@/components/work.vue';
import contact from '@/components/contact.vue';
import ggj from '@/components/work/ggj.vue';
import utiligroup from '@/components/work/utilitgroup.vue';
import vrhps from '@/components/work/vrhps.vue';
import steam from '@/components/work/steam.vue';
import pwc from '@/components/work/pwc.vue';
import wilko from '@/components/work/wilko.vue';
import gghlc from '@/components/work/gghlc.vue';
import datavis from '@/components/work/datavis.vue';
import localchamp from '@/components/work/localchampions.vue'

Vue.use(VueRouter);
//all routes
const routes = [
  {path: '/', name: 'home', component: home, meta: {title: 'Home'} },
  {path: '/about', name: 'about', component: about, meta: {title: 'About'} },
  {path: '/contact', name: 'contact', component: contact,  meta: {title: 'Contact Me'} },
 /** work
  * paths
  * 
  */
 {path: '/work', name: 'work', component: work, meta: {title: 'Portfolio'} },
// global games jam
 {path: '/work/ggj', name: 'ggj', component: ggj, meta: {title: 'Global Games Jam'}},
// utiligroup 
{path: '/work/utiligroup', name: 'utiligroup', component: utiligroup, meta: {title: 'Energy Utility Company'} },
//steam redesign and research 
{ path: '/work/steam', name: 'steam', component: steam, meta: {title: 'Steam'} },
//wilko
{ path: '/work/wilko', name: 'wilko', component: wilko, meta: {title: 'Wilko'} },
//gghlc
{ path: '/work/gghlc', name: 'gghlc', component: gghlc, meta: {title: 'Good Growth Hub X Local Champions'} },

//house plant sim
//{ path: '/work/vrhps', name: 'vrhps', component: vrhps, meta: {title: 'VR House Plant Simulator' } },
// pwc
{path: '/work/pwc', name: 'pwc', component: pwc, meta: {title: 'PwC X UK Black Tech Hackathon: AI for good'}
},

{path: '/work/pwc', name: 'datavis', component: datavis, meta: {title: 'Data Visualisation using Tableau'}
},
{path: '/work/pwc', name: 'localchamp', component: localchamp, meta: {title: 'Local Champions: Social Media Templates'}
},
,

];


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});



export default router;
