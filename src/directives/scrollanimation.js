
//animation for scroll on work elements in home and my work
const animatedScrollObserver = new IntersectionObserver(
    //if on page then use enter 
    (entries, animatedScrollObserver) => {
        entries.forEach((entry) => {
            if(entry.isIntersecting) {
               entry.target.classList.add('enter');
               animatedScrollObserver.unobserve(entry.target);
            }
        });
    }
);

//before on page do not enter
export default {
    bind(el) {
     el.classList.add('before-enter');
     animatedScrollObserver.observe(el);
     
    }
}

